# Pandas is imported as pd
import pandas as pd
# Numpy is imported as np
import numpy as np
# Matplotlib is Imported as plt
import matplotlib.pyplot as plt
# Seaborn is Imported as plt
import seaborn as sns
# Statistics is Imported 
import statistics 



# Utalising Pandas to read immunotherapy dataset
#Immunotherapy = pd.read_csv('Immunotherapy-G.csv', sep=',');



# 90 rows

Immunotherapy_1 = pd.read_csv('Immunotherapy.csv', sep=';');
Immunotherapy_1.to_csv('Immunotherapy_1.csv', index = False)

#360 rows
Immunotherapy_2=pd.DataFrame(pd.np.repeat(Immunotherapy_1.values,2,axis=0),columns=Immunotherapy_1.columns)

#Immunotherapy_2 = Immunotherapy_1.append([Immunotherapy_1]*2, ignore_index=True) 
Immunotherapy_2.to_csv('Immunotherapy_2.csv', index = False)
Immunotherapy_2 = pd.read_csv('Immunotherapy_2.csv', sep=',');





#1440 rows
#Immunotherapy_2.append([Immunotherapy_2]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_3= Immunotherapy_2.append([Immunotherapy_2]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_3.to_csv('Immunotherapy_3.csv', index = False)
Immunotherapy_3 = pd.read_csv('Immunotherapy_3.csv', sep=',');


#5760 rows
#Immunotherapy_3.append([Immunotherapy_3]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_4= Immunotherapy_3.append([Immunotherapy_3]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_4.to_csv('Immunotherapy_4.csv', index = False)
Immunotherapy_4 = pd.read_csv('Immunotherapy_4.csv', sep=',');

#23040 rows
#Immunotherapy_4.append([Immunotherapy_4]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_5= Immunotherapy_4.append([Immunotherapy_4]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_5.to_csv('Immunotherapy_5.csv', index = False)
Immunotherapy_5 = pd.read_csv('Immunotherapy_5.csv', sep=',');

# 92160 rows
#Immunotherapy_5.append([Immunotherapy_5]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_6 = Immunotherapy_5.append([Immunotherapy_5]*3, ignore_index=True) #Ignores the index as per your need
Immunotherapy_6.to_csv('Immunotherapy_6.csv', index = False)
Immunotherapy_6 = pd.read_csv('Immunotherapy_6.csv', sep=',');


#######################[ Chapter 3]#########################


# Counting number of bening and malignent tumours
y = Immunotherapy.Result_of_Treatment   
ax = sns.countplot(y,label="Count")
A, B = y.value_counts()
print('Results of immunotherapy treatment unsuccessful = ',B)
print('Results of treatment successful= ',A)

####################
# Immunotherapy data Information
Immunotherapy.info()
####################
# Boks plot for texture_se
bplot = sns.boxplot(y='Area', x='Result_of_Treatment',
                 data=Immunotherapy,
                 width=0.4,
                 palette="colorblind")
####################
# Immunotherapy data Statistical details 
Immunotherapy.describe()

####################
# The first few observations of the data
Immunotherapy.head()
####################
# Mode of the data
from statistics import mode 
statistics.mode(Immunotherapy)

#################### 
# Histogram of Area
plt.hist(Immunotherapy['Area'], bins = 4)
plt.xlabel('Area')
plt.ylabel('Count')
plt.title(' Histogram for Area')
plt.show()
#################### ok 
# Many histograms of the attributtes 
Immunotherapy.hist()
plt.show()
####################
# To drop the column called diagnosis and replace it with 
# a column of 0 and 1 called diagnosis_M
Immunotherapy= pd.get_dummies(Immunotherapy,drop_first=True) 

####################
# Correlation among all features
Immunotherapy.corr()

# Correlations of the attributtes with Result_of_Treatment
Immunotherapy.corr()['Result_of_Treatment'].sort_values()


# Scatter matrix
Scatter_Matrix = Immunotherapy.corr()
f, ax = plt.subplots(figsize =(10, 10))
sns.heatmap(Scatter_Matrix, ax = ax, cmap ="YlGnBu", linewidths = 0.2)
plt.show()
# A function showing most correlated features with the target feature diagnosis
# As well as the correlation coefficient

def cor_data(Immunotherapy,n):
   # Finding correlations with diagnosis
    most_correlated =Immunotherapy.corr().abs()['Result_of_Treatment'].sort_values(ascending=False)
    most_correlated = most_correlated[:n]
   # Returning most correlated variables
    return  most_correlated


#######################[ Chapter 4]#########################
####################
# Testing for missing data
Immunotherapy.isnull().sum()

####################
# Ploting radius_mean and texture_mean
sns.lmplot(x = 'Age', y = 'Number_of_Warts', hue = 'Result_of_Treatment', data = Immunotherapy) 

# Ploting smoothness_mean and compactness_mean
sns.lmplot(x = 'smoothness_mean', y = 'compactness_mean', hue = 'Result_of_Treatment', data = Immunotherapy_1) 

#######################[ Chapter 5]#########################

# Making five different datasets 





